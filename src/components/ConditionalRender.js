import React, { Component } from 'react'
import ConditionalChild from './ConditionalChild'

export default class ConditionalRender extends Component {
    constructor(){
        super()
        this.state = {
            isLoading: true
        }
    }

    componentDidMount(){
        setTimeout( () => {
            this.setState({
                isLoading: false
            })
        }, 1500)
    }

    render() {
        return (
            <div>
                {this.state.isLoading ? <h1>Loading...</h1> :
                <ConditionalChild >
                </ConditionalChild>}
            </div>
        )
    }
}
