import React from 'react'

export default function ProductCard(props) {
    return (

    <div className = 'container'>
      <div className='row'>
        <div className = {props.className}>
          <img variant="top" className='image' src={props.imgUrl} alt={props.className} />
            <p>{props.name}</p>
            <p>{props.price}</p>
        </div>
      </div>
    </div>
    )
}
