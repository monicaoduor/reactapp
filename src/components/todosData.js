const todosData = [
    {
        id: 1,
        text: "Take out the trash",
        completed: true
    },
    {
        id: 2,
        text: "Shopping",
        completed: false
    },
    {
        id: 3,
        text: "Workout",
        completed: false
    },
    {
        id: 4,
        text: "Tidy room",
        completed: true
    },
    {
        id: 5,
        text: "Catch up on You",
        completed: false
    },
    {
        id: 6,
        text: "Learn React",
        completed: false
    }
]

export default todosData