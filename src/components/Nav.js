import React from 'react';
import Login from './Login'

class Nav extends React.Component {

    constructor() {
        super()

        this.state = {
             unreadNotification: [
                'Cook supper',
                'Read Statistics'
             ]
        }
    }

        render(){
    return (
        <div className="topnav">
            <a className="active" href="#home">Home</a>
            <a href="#news">News</a>
            <a href="#contact">Contact</a>
            <a href="#about">About</a>
            <a href="#notification">
                <i className="fas fa-bell"></i>
                    {this.state.unreadNotification.length > 0 && <sub>{this.state.unreadNotification.length}</sub> }
            </a>
            <a href ="#login">
                <Login />
            </a>


        </div>
    )
}
}

export default Nav;