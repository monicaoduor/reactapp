import React from 'react'

export default function ConditionalChild(props) {
    const styles = {
        display: 'none'
    }
    return(
        <div>
            <h2 style={styles}>o</h2>
        </div>
    )
}
