import React, { Component } from 'react'

export default class LearningForms extends Component {
    constructor(){
        super()
        this.state = {
            firstName: '',
            lastName: '',
            isFriendly: !true,
            female: '',
            male: '',
            favColor: 'purple'
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange(event){
        const {name, value, type, checked} = event.target
        type ==='checkbox' ? this.setState({[name] : checked}) : this.setState({
            [name]: value
        })
    }
    //changes the state to reflect whatever the current value of the input value
    render() {
        return (
            <form onSubmit={this.handleSubmit} className='form text-center'>
                <h2>Forms</h2>
                <input
                    value={this.state.firstName}
                    type='text'
                    name='firstName'
                    placeholder='First Name'
                    onChange={this.handleChange}
                />
                <br />
                <input
                    name='lastName' type='text' placeholder='Last Name'
                    onChange={this.handleChange}
                />
                <br />
                <textarea
                    rows='2'
                    cols='22'
                    value={'Hi, welcome to my page'}
                    onChange={this.handleChange}
                />
                <br />
                <label>
                    <input
                        type='checkbox'
                        name='isFriendly'
                        checked={this.state.isFriendly}
                        onChange={this.handleChange}
                    /> Is Friendly?
                </label>
               <br />
               <label>
                    <input
                        type='radio'
                        name='gender'
                        value='female'
                        checked={this.state.gender === 'female'}
                        onChange={this.handleChange}
                    /> Female
                </label>
                <br />
               <label>
                    <input
                        type='radio'
                        name='gender'
                        value='male'
                        checked={this.state.gender === 'male'}
                        onChange={this.handleChange}
                    /> Male
                </label>
                <br />
                <label>Favorite color&nbsp;</label>
                <select
                    value={this.state.favColor}
                    onChange={this.handleChange}
                    name='favColor'
                    >
                    <option value='Red'>Red</option>
                    <option value='Orange'>Orange</option>
                    <option value='Yellow'>Yellow</option>
                    <option value='Green'>Green</option>
                    <option value='Blue'>Blue</option>
                    <option value='Indigo'>Indigo</option>
                    <option value='Violet'>Violet</option>
                </select>
                <p> {this.state.lastName} {this.state.firstName}</p>
                <p> Gender: {this.state.gender} </p>
                <p>Fav color: {this.state.favColor}  </p>
                <button >Submit</button>
            </form>
        )
    }
}
