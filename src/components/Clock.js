import React from 'react'

function Clock() {
    const date = new Date()
    const hours = date.getHours()
    const mins = date.getMinutes()
    let timeOfDay
    let amPm
    const styles = {
        fontSize : 20
    }
    const style = {
        color : 'purple',
        textDecoration: 'underline'
    }
    if(hours < 12){
        timeOfDay = 'morning'
        amPm = 'am'
    } else if (hours >= 12 && hours < 17){
        timeOfDay = 'afternoon'
        amPm = 'pm'
        styles.color = 'gray'
    } else {
        timeOfDay = 'evening'
        amPm = 'pm'
    }

    return(
        <div className = 'text-center'>
            <h2 style = {style}> Good {timeOfDay} </h2>
            <p style = {styles}>The time is {hours}:{mins} {amPm} </p>
        </div>
      )
}
export default Clock
