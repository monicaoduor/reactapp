const productsData = [
    {
        id : 1,
        imgUrl : 'https://images.unsplash.com/photo-1589001384171-4e4e6f21e761?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60',
        name : 'JBL Speaker',
        price : 'shs 14000',
        className : 'speaker col-4'
    },
    {
        id : 2,
        imgUrl : 'https://images.unsplash.com/photo-1552406923-bdd5268d3956?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60',
        name : 'JBL headphones',
        price : 'shs 6000',
        className : 'headphone col-4'
    },
    {
        id : 3,
        imgUrl : 'https://images.unsplash.com/photo-1552308995-2baac1ad5490?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60',
        name : 'Computer set-up',
        price : 'So much money 💵',
        className: 'computer col-4'
    }
]
export default productsData;