import React from 'react'
import productsData from './productsData'
import ProductCard from './ProductCard'

const Products = (props) => {
     // const nums = [1, 2, 3, 4, 5]
    // const doubled = nums.map(function(num){
    //     return num*2
    // })
    // console.log(doubled)

    const productComponent = productsData.map( (item) => <ProductCard key = {item.id} name = {item.name} price = {item.price} imgUrl = {item.imgUrl} className = {item.className}/>)
    return (
    <div>
        {productComponent}
    </div>

    )
}

export default Products