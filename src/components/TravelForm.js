import React, { Component } from 'react'

export default class TravelForm extends Component {
    constructor(){
        super()
        this.state = {
            firstName: '',
            lastName: '',
            age: '',
            gender: '',
            destination: '',
            lactose: true,
            vegan: false,
            kosher: false,
            wheat: false
        }
        this.handleChange = this.handleChange.bind(this)
    }
    handleChange(event){
        const {name, value, type, checked}= event.target
        type === 'checkbox' ?
        this.setState({
                [name]: checked

        }) : this.setState({
            [name]: value
        })
    }

    render() {
        const styles={
        textDecoration: 'underline',
        color: 'grey'
    }
       return(
        <div className="card card1 container">
        <div className="card-body">
          <h4 className="card-title">TRAVEL FORM</h4>
          <form>
              {/* firstName */}
              <input
                value={this.state.firstName}
                name='firstName'
                type='text'
                placeholder='First Name'
                onChange={this.handleChange}
              />
              <br /><br/>

              {/* lastName */}
              <input
              value={this.state.lastName}
                name='lastName'
                type='text'
                placeholder='Last Name'
                onChange={this.handleChange}
              />
              <br /><br/>

              {/* age */}
              <input
              value={this.state.age}
                name='age'
                type='text'
                placeholder='Age'
                onChange={this.handleChange}
              />
              <br /><br/>

              {/* Gender */}
              <strong style={styles}>Gender</strong><br />
              <label>
                    <input
                        type='radio'
                        name='gender'
                        value='Male'
                        checked={this.state.gender === 'male'}
                        onChange={this.handleChange}
                    /> Male
                </label>
                <br />
                <label>
                    <input
                        type='radio'
                        name='gender'
                        value='Female'
                        checked={this.state.gender === 'female'}
                        onChange={this.handleChange}
                    /> Female
                </label>
                <br /><br />

                {/* Destination */}
                <strong style={styles}>Destination</strong><br />
                <select
                    value={this.state.destination}
                    onChange={this.handleChange}
                    name='destination'>
                    <option value=''>Please choose destination</option>
                    <option value='Lodwar'>Lodwar</option>
                    <option value='Malindi'>Malindi</option>
                    <option value='Watamu'>Watamu</option>
                </select>
                <br /><br />

                {/* Dietary restrictions */}
                <strong style={styles}>Dietary restrictions</strong>
                <br />
                <label>
                    <input
                        type='checkbox'
                        name='Lactose'
                        checked={this.state.lactose}
                        onChange={this.handleChange}
                    /> Lactose intolerance
                </label><br/>
                <label>
                    <input
                        type='checkbox'
                        name='Kosher'
                        checked={this.state.kosher}
                        onChange={this.handleChange}
                    /> Kosher diet
                </label><br/>
                <label>
                    <input
                        type='checkbox'
                        name='Vegan'
                        checked={this.state.vegan}
                        onChange={this.handleChange}
                    /> Vegan diet
                </label><br/>
                <label>
                    <input
                        type='checkbox'
                        name='Wheat'
                        checked={this.state.wheat}
                        onChange={this.handleChange}
                    /> Wheat intolerance
                </label>
                <br /><br/>

              <button type='submit'>Submit</button>
              <hr/>

              {/* Entered information */}
              <h4>Entered information</h4>
                <ol>
                    <li>Your name: <strong>{this.state.firstName} {this.state.lastName}</strong></li>
                    <li>Your age: <strong>{this.state.age}</strong></li>
                    <li>Gender: <strong>{this.state.gender}</strong></li>
                    <li>Your destination: <strong>{this.state.destination}</strong></li>
                    <li>Dietary restrictions:</li>
                        <p>Vegan: <strong>{this.state.vegan ? 'Yes' : 'No'}</strong></p>
                        <p>Kosher: <strong>{this.state.kosher ? 'Yes' : 'No'}</strong></p>
                        <p>Wheat: <strong>{this.state.wheat ? 'Yes' : 'No'}</strong></p>
                        <p>Lactose: <strong>{this.state.lactose ? 'Yes' : 'No'}</strong></p>
                </ol>

          </form>
        </div>
      </div>

       )
    }
}
