import React from 'react'

class ToDoItem extends React.Component{
    render(){
    const styles = {
        textDecoration: 'line-through',
        color: 'gray'
    }
    return (
        <div className = 'todo-item'>
             <input
             type="checkbox"
             checked={this.props.items.completed}
             onChange={() => this.props.handleChange(this.props.items.id)}
             />
             <label style={this.props.items.completed ? styles : null}>{this.props.items.text}</label>
        </div>
    )
}
}
export default ToDoItem