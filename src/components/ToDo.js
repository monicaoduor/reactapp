import React from 'react'
import ToDoItem from './ToDoItem'
import todosData from './todosData'

class ToDo extends React.Component {
    constructor(props){
        super(props)
        this.state = {
            todos : todosData,
        }
    }

    handleChange = (id) => {
        this.setState( (prevState) => {
            const newToDos = prevState.todos.map( (toDoItem) => {
                if (toDoItem.id === id){
                    toDoItem.completed = !toDoItem.completed
                }
                return toDoItem
            })
            return {
                todos : newToDos
            }
        })

    }
    render() {
        const todoItems = this.state.todos.map((item) =>
            <ToDoItem key = {item.id} items =  {item} handleChange = {this.handleChange}/>
    )

        return (
            <div className="to-do-list">
                <h2>To do list</h2>
                    {todoItems}
            </div>
        )
    }
}
export default ToDo;