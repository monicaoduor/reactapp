import React, { Component } from 'react'

class Login extends Component {
    constructor (){
        super ()
        this.state = {
            isLoggedIn : !false,
        }
        this.handleClick = this.handleClick.bind(this)
    }

    handleClick(){
        this.setState( (prevState) => {
            return {
               isLoggedIn: !prevState.isLoggedIn
            }
        })
    }

    render() {
        let wordDisplay
        this.state.isLoggedIn === true ?wordDisplay = 'In' :  wordDisplay = 'Out'
        return (
            <div>
                <button className = 'btn btn-lg login-btn' onClick={this.handleClick}>Log {wordDisplay}
                </button>
            </div>
        )
    }
}
export default Login