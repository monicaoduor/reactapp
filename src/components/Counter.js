import React from 'react'

class Counter extends React.Component {
    constructor(){
        super()
        this.state = {
            count: 0,
            color: ''
        }
        this.handleClick = this.handleClick.bind(this)
        this.undoClick = this.undoClick.bind(this)
        this.reset = this.reset.bind(this)
    }

    handleClick() {
        this.setState( (prevState) => {
            return {
                count: prevState.count + 1
            }
        } )
    }

    undoClick(){
        this.setState ( (prevState) => {
            return {
                count: prevState.count - 1
            }
        })
    }

    reset(){
        this.setState({count : 0})
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.count !== this.state.count){
        const newColor = 'red'
        this.setState({color: newColor})
        }
    }

    render() {
        return (
            <div className ='text-center'>
                <h2 className = 'counter-head'>Counter</h2>
                <h2 style={{color: this.state.color}} >{this.state.count}</h2>
                <button className='btn btn-lg' onClick={this.handleClick}>Increment</button>
                <br />
                <button className= 'btn btn-sm'onClick={this.undoClick}>
                    Decrement
                </button>
                <button className= 'btn btn-sm'onClick={this.reset}>
                    Reset
                </button>
            </div>
        )
    }
}
export default Counter