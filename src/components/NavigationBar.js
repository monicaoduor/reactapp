import React from 'react';
import Login from './Login'


class Nav extends React.Component {

    constructor(props) {
        super(props)

        this.state = {
             display : 'd-none',
             unreadNotification: [
                'Cook supper',
                'Read Statistics'
             ]
        }
    }

        render(){
    return (
        <div className = 'topnav'>
            <nav className="navbar navbar-expand-lg navbar-light">
                <a className="navbar-brand" href="#0">Navbar</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="navbar-collapse collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <a className="nav-link" href='#0' >Home <span className="sr-only">(current)</span></a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" href='#0' >
                                    <i className="fas fa-bell"></i>
                                        {this.state.unreadNotification.length > 0 && <sub>{this.state.unreadNotification.length}</sub> }</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link disabled" href='#0'>Disabled</a>
                            </li>
                            </ul>
                            <div>
                                <Login />
                            </div>

                    </div>
            </nav>
        </div>

    )
}
}

export default Nav;